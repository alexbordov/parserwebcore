﻿namespace ParserWebCore.BuilderApp
{
    public enum Arguments
    {
        Agrocomplex,
        Kzgroup,
        Agrotomsk,
        Sibintek,
        Setonline,
        Mzvoron,
        Maxi,
        Tver,
        Murman,
        Kalug,
        Smol,
        Samar,
        Udmurt,
        Segezha,
        Akashevo
    }
}